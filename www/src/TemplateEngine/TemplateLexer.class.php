<?php

namespace TemplateEngine;

use TemplateEngine\Error\SyntaxError;

class TemplateLexer 
{
    private const EOF_STATE = -1;
    private const TEXT_STATE = 0;
    private const BLOCK_STATE = 1;
    private const EXPRESSION_STATE = 2;
    private const STRING_STATE = 3;
    private const COMMENT_STATE = 4;

    private $state;
    private $offset;
    private $tokens;
    private $source;
    private $code;
    private $regexes;
    private $options;
    private $line;

    public function __construct($env, array $options)
    {
        $this->options = [];
        if (isset($env))
            $this->options = array_merge($this->options, $env->getOption('template_lexer'));
        if (isset($options))
            $this->options = array_merge($this->options, $options);

        $this->regexes = [];
        $this->regexes['whitespaces'] = $this->craftWhitespacesRegex();
        $this->regexes['block_start'] = $this->craftBlockStartRegex();
        $this->regexes['block_end'] = $this->craftBlockEndRegex();
        $this->regexes['expression_start'] = $this->craftExpressionStartRegex();
        $this->regexes['expression_end'] = $this->craftExpressionEndRegex();
        $this->regexes['comment_start'] = $this->craftCommentStartRegex();
        $this->regexes['comment_end'] = $this->craftCommentEndRegex();
        $this->regexes['number'] = $this->craftNumberRegex();
        $this->regexes['name'] = $this->craftNameRegex();
        $this->regexes['operators'] = $this->craftOperatorsRegex();
        $this->regexes['punctuation'] = $this->craftPunctuationRegex();
        $this->regexes['raw_string'] = $this->craftRawStringRegex();
    }

    public function tokenize($source)
    {
        set_time_limit(20);
        $this->source = $source;
        $this->tokens = [];
        $this->state = self::TEXT_STATE;
        $this->offset = 0;
        $this->code = $this->source->getCode();
        $this->line = 0;

        while ($this->offset < strlen($this->code)) {
            switch ($this->state) {
                case self::TEXT_STATE:
                    $this->processText();
                    break;
                case self::BLOCK_STATE:
                    $this->processBlock();
                    break;
                case self::EXPRESSION_STATE:
                    $this->processExpressionBlock();
                    break;
                case self::COMMENT_STATE:
                    $this->processComment();
                    break;
            }
            if ($this->state === self::EOF_STATE)
                break;
        }
        $this->appendToken(new Token(Token::EOF_TYPE, '', $this->line));
        return new TokenStream($this->tokens, $source);
    }

    private function processText()
    {
        $regexStart = '/' . \implode('|', [
            $this->regexes['block_start'],
            $this->regexes['expression_start'],
            $this->regexes['comment_start']
            ]) . '/';
        \preg_match($regexStart, $this->code, $matches, PREG_OFFSET_CAPTURE, $this->offset);
        
        if (!empty($matches)) {
            $startValue = $matches[0][0];
            $startOffset = $matches[0][1];
        } else {
            $startOffset = \strlen($this->code);
        }

        $textValue = \substr($this->code, $this->offset, $startOffset - $this->offset);
        if (\strlen($textValue) !== 0) {
            $this->appendToken(new Token(Token::TEXT_TYPE, $textValue, $this->line));
            $this->moveCurrentPosition($textValue);
        }
        
        if (!empty($matches)) {
            $this->moveCurrentPosition($startValue);
            if (\preg_match('/' . $this->regexes['block_start'] . '/', $startValue)) {
                $this->appendToken(new Token(Token::BLOCK_START_TYPE, $startValue, $this->line));
                $this->state = self::BLOCK_STATE;
            } else if (\preg_match('/' . $this->regexes['expression_start'] . '/', $startValue)) {
                $this->appendToken(new Token(Token::EXPRESSION_START_TYPE, $startValue, $this->line));
                $this->state = self::EXPRESSION_STATE;
            } else if (\preg_match('/' . $this->regexes['comment_start'] . '/', $startValue)) {
                $this->state = self::COMMENT_STATE;
            }
        } else {
            $this->state = self::EOF_STATE;
        }
    }

    private function processBlock()
    {
        if (\preg_match('/(' . $this->regexes['whitespaces'] . ')*/A', $this->code, $matches, 0, $this->offset))
            $this->moveCurrentPosition($matches[0]);

        if (\preg_match('/' . $this->regexes['block_end'] . '/A', $this->code, $matches, 0, $this->offset)) {
            $this->moveCurrentPosition($matches[0]);
            $this->state = self::TEXT_STATE;
            $this->appendToken(new Token(Token::BLOCK_END_TYPE, $matches[0], $this->line));
        }
        else
            $this->processExpressionParts();
    }

    private function processExpressionBlock()
    {
        if (\preg_match('/(' . $this->regexes['whitespaces'] . ')*/A', $this->code, $matches, 0, $this->offset))
            $this->moveCurrentPosition($matches[0]);

        if (\preg_match('/' . $this->regexes['expression_end'] . '/A', $this->code, $matches, 0, $this->offset)) {
            $this->moveCurrentPosition($matches[0]);
            $this->state = self::TEXT_STATE;
            $this->appendToken(new Token(Token::EXPRESSION_END_TYPE, $matches[0], $this->line));
        }
        else
            $this->processExpressionParts();
    }

    private function processExpressionParts()
    {
        if (\preg_match('/' . $this->regexes['operators'] . '/A', $this->code, $matches, 0, $this->offset)) {
            $this->appendToken(new Token(Token::OPERATOR_TYPE, $matches[0], $this->line));
            $this->moveCurrentPosition($matches[0]);
        }
        elseif (\preg_match('/' . $this->regexes['punctuation'] . '/A', $this->code, $matches, 0, $this->offset)) {
            $this->appendToken(new Token(Token::PUNCTUATION_TYPE, $matches[0], $this->line));
            $this->moveCurrentPosition($matches[0]);
        }
        elseif (\preg_match('/' . $this->regexes['name'] . '/A', $this->code, $matches, 0, $this->offset)) {
            $this->appendToken(new Token(Token::NAME_TYPE, $matches[0], $this->line));
            $this->moveCurrentPosition($matches[0]);
        }
        elseif (\preg_match('/' . $this->regexes['number'] . '/A', $this->code, $matches, 0, $this->offset)) {
            $number = 
                \filter_var($matches[0], FILTER_VALIDATE_INT) ?:
                \filter_var($matches[0], FILTER_VALIDATE_FLOAT);
            $this->appendToken(new Token(Token::NUMBER_TYPE, $number, $this->line));
            $this->moveCurrentPosition($matches[0]);
        }
        elseif (\preg_match('/' . $this->regexes['raw_string'] . '/A', $this->code, $matches, 0, $this->offset)) {
            $stringValue = $matches[1];
            $this->appendToken(new Token(Token::STRING_TYPE, $stringValue, $this->line));
            $this->moveCurrentPosition($matches[0]);
        } 
        else {
            throw new SyntaxError("Unexpected character \"{$this->code[$this->offset]}\"", $this->line, $this->source);
        }
    }

    private function processComment()
    {
        \preg_match('/' . $this->regexes['comment_end'] . '/', $this->code, $matches, PREG_OFFSET_CAPTURE, $this->offset);
        if (empty($matches))
            throw new SyntaxError('Unexpected end of template', $this->line, $this->source);
    
        list($endValue, $endOffset) = $matches[0];
        $commentValue = \substr($this->code, $this->offset, $endOffset - $this->offset);
        $this->moveCurrentPosition($commentValue);
        $this->moveCurrentPosition($endValue);
        if ($this->offset == strlen($this->code)) {
            $this->state = self::EOF_STATE;
            $this->appendToken(new Token(Token::EOF_TYPE, '', $this->line));
        } else {
            $this->state = self::TEXT_STATE;
        }
    }

    private function appendToken(Token $token)
    {
        array_push($this->tokens, $token);
        $tokenCount = count($this->tokens);
        if ($tokenCount >= 2) {
            if ($this->tokens[$tokenCount - 1]->getType() === Token::TEXT_TYPE && 
                $this->tokens[$tokenCount - 2]->getType() === Token::TEXT_TYPE) 
            {
                $tokenValue = 
                    $this->tokens[$tokenCount - 2]->getValue() .
                    $this->tokens[$tokenCount - 1]->getValue();
                $token = new Token(Token::TEXT_TYPE, $tokenValue, $this->tokens[$tokenCount - 1]->getLine());
                array_pop($this->tokens);
                array_pop($this->tokens);
                array_push($this->tokens, $token);
            }
        }
    }

    private function moveCurrentPosition($text)
    {
        $this->offset += \strlen($text);
        $this->line += \substr_count($text, "\n");
    }

    private function craftRawStringRegex()
    {
        $regex = '';
        $regex .= preg_quote($this->options['raw_string_quote']);
        $regex .= '(';
        $regex .= '(?:[^\\\\' . preg_quote($this->options['raw_string_quote']) . ']|\\\\.)*';
        $regex .= '(?!\\\\' . preg_quote($this->options['raw_string_quote']) . ')';
        $regex .= ')';
        $regex .= preg_quote($this->options['raw_string_quote']);
        return $regex;
    }

    private function craftWhitespacesRegex()
    {
        return $this->craftAbstractListRegex($this->options['whitespaces'], false);
    }

    private function craftNameRegex()
    {
        return $this->options['name_regex'];
    }

    private function craftNumberRegex()
    {
        return $this->options['number_regex'];
    }

    private function craftPunctuationRegex()
    {
        return $this->craftAbstractListRegex($this->options['punctuation']);
    }

    private function craftOperatorsRegex()
    {
        $operators = array_merge(
            $this->options['unary_operators'], 
            $this->options['binary_operators']);
        $regex = $this->craftAbstractListRegex($operators);
        return $regex;
    }

    private function craftBlockStartRegex()
    {
        return $this->craftAbstractStartRegex($this->options['block_brackets'][0]);
    }

    private function craftBlockEndRegex()
    {
        return $this->craftAbstractEndRegex($this->options['block_brackets'][1]);
    }

    private function craftExpressionStartRegex()
    {
        return $this->craftAbstractStartRegex($this->options['expression_brackets'][0]);
    }

    private function craftExpressionEndRegex()
    {
        return $this->craftAbstractEndRegex($this->options['expression_brackets'][1]);
    }

    private function craftCommentStartRegex()
    {
        return $this->craftAbstractStartRegex($this->options['comment_brackets'][0]);
    }

    private function craftCommentEndRegex()
    {
        return $this->craftAbstractEndRegex($this->options['comment_brackets'][1]);
    }

    private function craftAbstractStartRegex($startPattern)
    {
        $regex = '';
        $regex .= 
            '(' . $this->regexes['whitespaces'] . ')*' . 
            preg_quote($startPattern, '/') . 
            preg_quote($this->options['whitespace_trim']);
        $regex .= '|';
        $regex .= preg_quote($startPattern, '/');
        return $regex;
    }

    private function craftAbstractEndRegex($endPattern)
    {
        $regex = '(';
        $regex .= 
            preg_quote($this->options['whitespace_trim']) . 
            preg_quote($endPattern, '/') . 
            '(' . $this->regexes['whitespaces'] . ')*';
        $regex .= '|';
        $regex .= preg_quote($endPattern, '/');
        $regex .= ")\n?";
        return $regex;
    }

    private function craftAbstractListRegex($regexList, $quote = true)
    {
        $regex = '';
        $quoter = function ($text) {
            return preg_quote($text, '/');
        };
        $comparator = function ($a, $b) {
            return strlen($b) - strlen($a);
        };
        usort($regexList, $comparator);
        if ($quote)
            $regexList = array_map($quoter, $regexList);
        $regex .= implode('|', $regexList);
        return $regex;
    }
}
