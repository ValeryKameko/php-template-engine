<?php

namespace TemplateEngine\NodeParser;

use TemplateEngine\Parser;
use TemplateEngine\Token;
use TemplateEngine\Node\IncludeBlockNode;

class IncludeBlockNodeParser extends AbstractNodeParser
{
    public function __construct()
    {

    }

    public function subparse(Parser $parser)
    {
        $line = $parser->getLine();
        $parser->expect(Token::NAME_TYPE, 'include');
        $includeFilePathExpression = $parser->parseExpression();
        return new IncludeBlockNode($includeFilePathExpression, $line);
    }
}