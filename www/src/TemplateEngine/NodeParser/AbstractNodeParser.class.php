<?php

namespace TemplateEngine\NodeParser;

use TemplateEngine\Parser;

abstract class AbstractNodeParser
{
    public function __construct()
    {

    }

    abstract public function subparse(Parser $parser);
}