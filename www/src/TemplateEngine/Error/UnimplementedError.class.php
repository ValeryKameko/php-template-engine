<?php

namespace TemplateEngine\Error;

class UnimplementedError extends \Exception
{
    public $rawMessage;
    public $source;
    public $line;
    
    public function __construct($rawMessage, $line = NULL, $source = NULL, $previous = NULL)
    {
        parent::__construct('', 0, $previous);
        $this->rawMessage = $rawMessage;
        $this->line = $line;
        $this->source = $source;
        $this->updateMessage();
    }

    public function updateMessage()
    {
        $message = '';
        $message .= $this->rawMessage;
        if (!is_null($this->source))
            $message .= ' is template ' . $this->source->getName();
        if (!is_null($this->line))
            $message .= ' in line ' . $this->line;
        $this->message = $message;
    }
}