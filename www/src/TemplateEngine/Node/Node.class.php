<?php

namespace TemplateEngine\Node;

use TemplateEngine\Compiler;

class Node implements ICompilable
{
    protected $nodes;
    protected $attributes;
    protected $line;
    protected $tag;

    private $name;

    public function __construct($nodes, $attributes, $line, $tag)
    {
        $this->nodes = $nodes;
        $this->attributes = $attributes;
        $this->line = $line;
        $this->tag = $tag;
    }

    public function compile(Compiler $compiler)
    {
        foreach ($this->nodes as $node) {
            $node->compile($compiler);
        }
    }

    public function setNode($name, $node)
    {
        $this->nodes[$name] = $node;
    }

    public function removeNode($name)
    {
        unset($this->nodes[$name]);
    }

    public function getNode($name)
    {
        return $this->nodes[$name];
    }

    public function getNodes()
    {
        return $this->nodes;
    }

    public function setAttribute($attribute, $value)
    {
        $this->attributes[$attribute] = $value;
    }

    public function removeAttribute($attribute)
    {
        unset($this->attributes[$attribute]);
    }

    public function getAttribute($attribute)
    {
        return $this->attributes[$attribute];
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        foreach ($nodes as $nodeName => $node) {
            $node->setName($name);
        }
    }

    public function getLine()
    {
        return $this->line;
    }

    public function setLine($line)
    {
        $this->line = $line;
    }

    public function getTag()
    {
        return $this->tag;
    }
}