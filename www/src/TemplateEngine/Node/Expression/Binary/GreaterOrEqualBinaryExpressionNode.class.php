<?php

namespace TemplateEngine\Node\Expression\Binary;

use TemplateEngine\Compiler;

class GreaterOrEqualBinaryExpressionNode extends AbstractBinaryExpressionNode
{
    public function compileOperator(Compiler $compiler)
    {
        $compiler->write('>=');
    }
}