<?php

namespace TemplateEngine\Node\Expression\Binary;

use TemplateEngine\Compiler;

class ConcatBinaryExpressionNode extends AbstractBinaryExpressioNode
{
    public function compileOperator(Compiler $compiler)
    {
        $compiler->write('.');
    }
}