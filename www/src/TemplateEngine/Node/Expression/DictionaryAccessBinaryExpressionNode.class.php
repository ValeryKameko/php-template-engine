<?php

namespace TemplateEngine\Node\Expression;

use TemplateEngine\Compiler;

class DictionaryAccessBinaryExpressionNode extends AbstractExpressionNode
{
    public function __construct($mapExpression, $keyExpression, $line)
    {
        parent::__construct([ 'map' => $mapExpression, 'key' => $keyExpression ], [], $line, '');
    }

    public function compile(Compiler $compiler)
    {
        $compiler->write('( ');
        $this->nodes['map']->compile($compiler);
        $compiler->write('[ ');
        $this->nodes['key']->compile($compiler);
        $compiler->write(' ]');
        $compiler->write(' )');
    }
}