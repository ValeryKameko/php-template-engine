<?php

namespace TemplateEngine\Node;

use TemplateEngine\Compiler;

interface ICompilable
{
    public function compile(Compiler $compile);
}
