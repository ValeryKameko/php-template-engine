<?php

namespace TemplateEngine\Node;

use TemplateEngine\Compiler;
use TemplateEngine\Error\UnimlementedError;

class DBIncludeBlockNode extends Node
{
    public function __construct($dbIncludeExpressionNode, $line)
    {
        parent::__construct([ 'db_include_expression_node' => $dbIncludeExpressionNode ], [], $line, 'db_include');
    }

    public function compile(Compiler $compiler)
    {
        $compiler->write('$this->displayDBVariable(');
        $this->nodes['db_include_expression_node']->compile($compiler);
        $compiler->writeLine(', $context);');
    }
}