<?php

namespace TemplateEngine\Node;

use TemplateEngine\Compiler;
use TemplateEngine\Error\UnimlementedError;

class ConfigIncludeBlockNode extends Node
{
    public function __construct($configInclueExpressionNode, $line)
    {
        parent::__construct([ 'config_include_expression_node' => $configInclueExpressionNode ], [], $line, 'include');
    }

    public function compile(Compiler $compiler)
    {
        $compiler->write('$this->displayConfigVariable(');
        $this->nodes['config_include_expression_node']->compile($compiler);
        $compiler->writeLine(', $context);');
    }
}