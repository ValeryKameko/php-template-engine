<?php

class Autoloader 
{
    protected static $directorySeparator = DIRECTORY_SEPARATOR;
    protected static $classPathSeparator = '\\';
    protected static $baseDirectory = __DIR__;
    protected static $prefixes = [];

    private static function concatFilePaths(string ...$filePaths) : string
    {
        return implode(static::$directorySeparator, $filePaths);
    }

    private static function classPathToFilePath($suffix)
    {
        return str_replace(static::$classPathSeparator, static::$directorySeparator, $suffix);
    }

    protected static function tryLoadFile(string $filePath) : bool
    {
        if (file_exists($filePath)) {       
            include_once $filePath;
            return true;
        }
        return false;
    }

    protected static function tryLoadItem(string $prefix, string $item) : bool
    {
        if (!isset(static::$prefixes[$prefix]))
            return false;
        $itemPath = self::classPathToFilePath($item);
        foreach (static::$prefixes[$prefix] as $subPath) {
            $filePath = static::concatFilePaths(static::$baseDirectory, $subPath, "$itemPath.class.php");
            if (static::tryLoadFile($filePath))
                return true;
        }
        return false;
    }

    public static function loadClass(string $className) : bool
    {
        $classItems = explode(static::$classPathSeparator, static::normalizeClassPath($className));
        $prefixItems = $classItems;
        $suffixItems = [];
        while (true) {
            $prefixPath = implode(static::$classPathSeparator, $prefixItems);
            $itemPath = implode(static::$classPathSeparator, $suffixItems);

            if (static::tryLoadItem($prefixPath, $itemPath))
                return true;
            if (empty($prefixItems))
                return false;

            $item = array_pop($prefixItems);
            array_unshift($suffixItems, $item);
        }
        return false;
    }

    public static function register() 
    {
        spl_autoload_register('Autoloader::loadClass');
    }
    
    public static function unregister()
    {
        spl_autoload_unregister('Autoloader::loadClass');
    }

    public static function addPrefixes(array $prefixes)
    {
        foreach ($prefixes as $prefix => $paths) {
            foreach ($paths as $path) {
                static::addPrefix($prefix, $path);
            }
        }
    }

    public static function addPrefix(string $prefix, string $directory)
    {
        static::$prefixes[$prefix] = static::$prefixes[$prefix] ?? [];
        array_push(static::$prefixes[$prefix], $directory);
    }

    public static function removePrefix(string $prefix) : string
    {
        unset(static::$prefixes[$prefix]);
    }

    public static function setBaseDirectory(string $baseDirectory)
    {
        static::$baseDirectory = static::normalizeFilePath($baseDirectory);
    }

    protected static function normalizeFilePath(string $path) : string
    {
        return rtrim($path, static::$directorySeparator);
    }

    protected static function normalizeClassPath(string $path) : string
    {
        return rtrim($path, static::$classPathSeparator);
    }
}