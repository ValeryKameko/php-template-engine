<?php
include_once 'src/Autoloader.php';

Autoloader::setBaseDirectory(__DIR__);
Autoloader::addPrefix('TemplateEngine', 'src/TemplateEngine');
Autoloader::register();

$loader = new TemplateEngine\FilesystemTemplateLoader(__DIR__ . '/template/');

$env = new TemplateEngine\Environment($loader, []);

$kek = new TemplateEngine\NodeParser\IncludeBlockNodeParser();

var_dump($env->compile('test.tmpl'));
$template = $env->load('test.tmpl');
$template->display([ 
    "kek" => "kek_lol",
    "list" => [1, 2, 3, 4, 5, 6, 7],
    "db_provider" => new TemplateEngine\DBProvider\JsonDBProvider(__DIR__ . '/data/data.json')
]);